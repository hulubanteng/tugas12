<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http.equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, inital-scale=1.0">
</head>
<body>
    <h1>Sign Up Form</h1>
       <form action="/welcome" method="POST">
        @csrf

        <label>First name:</label><br>
        <input type="text" name="fname"><br><br>
        
        <label>Last name :</label><br>
        <input type="text" name="lname"><br><br>

        <label>Gender :</label><br>
        <input type="radio"> Male<br><br>
        <input type="radio"> Female<br><br>

        <label>Nasionality :</label><br>
        <select name="Nasionality" id="Nasionality">
            <option value="Indonesia">Indonesia</option>
            <option value="Other">Other</option>
        </select> <br><br>

        <label>Language Spoken :</label><br>
        <input type="checkbox" name="skill">Bahasa Indonesia<br><br>
        <input type="checkbox" name="skill">English<br><br>
        <input type="checkbox" name="skill">Other<br><br>

        <label>Bio :</label><br>
        <textarea name="message" rows="10" cols="30"></textarea><br><br>

        <input type="submit" value="Submit">

</body>
</html>